package bookstoread;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("<= A BookShelf =>")
public class BookShelfSpec {

  private BookShelf shelf;
  private Book effectiveJava;
  private Book codeComplete;
  private Book mythicalManMonth;
  private Book thePhoenixProject;
  private Book java8InAction;
  private Book cleanCode;

  private BookShelfSpec(TestInfo testInfo) {
    System.out.println("Working on test " + testInfo.getDisplayName());
  }

  @BeforeEach
  void init() throws Exception {
    shelf = new BookShelf();
    effectiveJava = new Book("Effective Java", "Joshua Bloch",
            LocalDate.of(2008, Month.MAY, 8));
    codeComplete = new Book("Code Complete", "Steve McConnel",
            LocalDate.of(2004, Month.JUNE, 9));
    mythicalManMonth = new Book("The Mythical Man-Month",
            "Frederick Phillips Brooks",
            LocalDate.of(1975, Month.JANUARY, 1));
    thePhoenixProject = new Book("The Phoenix Project", "Antonio Cassano",
            LocalDate.of(1984, Month.JULY, 13));
    java8InAction = new Book("Java 8 in Action", "Antonio Cassano",
            LocalDate.of(2017, Month.APRIL, 1));
    cleanCode = new Book("Clean Code", "Robert C. Martin",
            LocalDate.of(2008, Month.JULY, 1));
  }

  @Test
  @DisplayName("<= Is Empty When No Book Added =>")
  void shelfEmptyWhenNoBookAdded(TestInfo testInfo) throws Exception {
    System.out.println("Working on test case " + testInfo.getDisplayName());
    List<Book> books = shelf.books();
    assertTrue(books.isEmpty(), () -> "BookShelf should be empty.");
  }

  @Test
  @DisplayName("<= Contains Two Books When Two Books Added =>")
  void bookshelfContainsTwoBooksWhenTwoBooksAdded() {
    shelf.add(effectiveJava, codeComplete);
    List<Book> books = shelf.books();
    assertEquals(2, books.size(), () -> "BookShelf should have two books.");
  }

  @Test
  @DisplayName("<= Should Print Book Count And Titles =>")
  void shelfToStringShouldPrintBookCountAndTitles() throws Exception {
    List<Book> books = shelf.books();
    shelf.add(thePhoenixProject);
    shelf.add(java8InAction);
    String shelfStr = shelf.toString();
    assertTrue(shelfStr.contains("The Phoenix Project"), () -> "1st book title missing");
    assertTrue(shelfStr.contains("Java 8 in Action") , () -> "2nd book title missing ");
    assertTrue(shelfStr.contains("2 books found"), () -> "Book count missing");
  }

  @Test
  @DisplayName("<= Is Empty When Add Is Called Without Books =>")
  void emptyBookShelfWhenAddIsCalledWithoutBooks() {
    shelf.add();
    List<Book> books = shelf.books();
    assertTrue(books.isEmpty(), () -> "BookShelf should be empty.");
  }

  @Test
  @DisplayName("<= Is Immutable For Client =>")
  void booksReturnedFromBookShelfIsImmutableForClient() {
    shelf.add(effectiveJava, codeComplete);
    List<Book> books = shelf.books();
    try {
      books.add(mythicalManMonth);
      fail(() -> "Should not be able to add book to books");
    } catch (Exception e) {
      assertTrue(e instanceof UnsupportedOperationException, () -> "Should throw UnsupportedOperationException.");
    }
  }

  @Test
  @DisplayName("<= Is Arranged By Book Title =>")
  void bookshelfArrangedByBookTitle() {
    shelf.add(effectiveJava, codeComplete, mythicalManMonth);
    List<Book> books = shelf.arrange();
    assertEquals(Arrays.asList( codeComplete, effectiveJava, mythicalManMonth),
            books, () -> "Books in a bookshelf should be arranged lexicographically by book title");
  }

  @Test
  @DisplayName("<= Books Are In Insertion Order After Calling Arrange =>")
  void booksInBookShelfAreInInsertionOrderAfterCallingArrange() {
    shelf.add(effectiveJava, codeComplete, mythicalManMonth);
    shelf.arrange();
    List<Book> books = shelf.books();
    assertEquals(Arrays.asList(effectiveJava, codeComplete, mythicalManMonth),
            books, () -> "Books in bookshelf are in insertion order");
  }

  @Test
  @DisplayName("<= Books Arranged By User Provided Criteria =>")
  void bookshelfArrangedByUserProvidedCriteria() {
    shelf.add(effectiveJava, codeComplete, mythicalManMonth);
    List<Book> books = shelf.arrange(Comparator.<Book>naturalOrder().reversed());
    assertEquals(Arrays.asList(mythicalManMonth, effectiveJava, codeComplete), books,
            () -> "Books in a bookshelf are arranged in descending order of book title");
  }

  @Test
  @DisplayName("books inside bookshelf are grouped by publication year")
  void groupBooksInsideBookShelfByPublicationYear() {
    shelf.add(effectiveJava, codeComplete, mythicalManMonth, cleanCode);
    Map<Year, List<Book>> booksByPublicationYear = shelf.groupByPublicationYear();
    assertThat(booksByPublicationYear).containsKey(Year.of(2008))
            .containsValues(Arrays.asList(effectiveJava, cleanCode));
    assertThat(booksByPublicationYear).containsKey(Year.of(2004))
            .containsValues(singletonList(codeComplete));
    assertThat(booksByPublicationYear).containsKey(Year.of(1975))
            .containsValues(singletonList(mythicalManMonth));
  }

  @Test
  @DisplayName("books inside bookshelf are grouped according to user provided criteria(group by author name)")
  void groupBooksByUserProvidedCriteria() {
    shelf.add(effectiveJava, codeComplete, mythicalManMonth, cleanCode);
    Map<String, List<Book>> booksByAuthor = shelf.groupBy(Book::getAuthor);
    assertThat(booksByAuthor)
            .containsKey("Joshua Bloch")
            .containsValues(singletonList(effectiveJava));
    assertThat(booksByAuthor)
            .containsKey("Steve McConnel")
            .containsValues(singletonList(codeComplete));
    assertThat(booksByAuthor)
            .containsKey("Frederick Phillips Brooks")
            .containsValues(singletonList(mythicalManMonth));
    assertThat(booksByAuthor)
            .containsKey("Robert C. Martin")
            .containsValues(singletonList(cleanCode));
  }

}
